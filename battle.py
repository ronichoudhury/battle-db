import argparse
import sqlalchemy.ext.declarative
from sqlalchemy import Column
from sqlalchemy import Float
from sqlalchemy import Integer
from sqlalchemy import String
import random
import string
import time

import battledb

Base = sqlalchemy.ext.declarative.declarative_base()


class Person(Base):
    __tablename__ = "people"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)
    income = Column(Float)


class Timer(object):
    def __enter__(self):
        self.start = time.time()

    def __exit__(self, *_):
        self.finish = time.time()

    def seconds(self):
        return self.finish - self.start

    def elapsed(self):
        return time.time() - self.start


def random_name():
    name = random.choice(string.ascii_uppercase)
    for _ in xrange(random.randint(4, 10)):
        name += random.choice(string.ascii_lowercase)

    return name


def random_age():
    return random.randint(1, 100)


def random_income():
    return random.uniform(0.0, 120000.0)


def create_sql_db(dbtype, filename, total, timer):
    random.seed(0)

    s = battledb.SQL(dbtype, filename, Person)
    s.connect()
    Base.metadata.create_all(bind=s.engine)

    for i in xrange(total):
        s.insert(Person(name=random_name(), age=random_age(), income=random_income()))

        if i % (512 * 1024) == 0:
            s.commit()
            print >>sys.stderr, "processed %d of %d records (%g seconds)" % (i + 1, total, timer.elapsed())

    s.commit()


def create_mongo_db(total, timer):
    random.seed(0)

    s = battledb.Mongo()
    s.connect()

    for i in xrange(total):
        s.insert({"name": random_name(),
                  "age": random_age(),
                  "income": random_income()})

        if i % (512 * 1024) == 0:
            s.commit()
            print >>sys.stderr, "processed %d of %d records (%g seconds)" % (i + 1, total, timer.elapsed())

    s.commit()


def query_sql_db(dbtype, filename, conditions):
    s = battledb.SQL(dbtype, filename, Person)
    s.connect()

    return s.query(*conditions)


def query_mongo_db(conditions):
    s = battledb.Mongo()
    s.connect()

    return s.query(*conditions)


def main():
    p = argparse.ArgumentParser(description="speed test for different databases")
    p.add_argument("-c", "--create", type=str, metavar="DBTYPE", default=None, help="create a database")
    p.add_argument("-q", "--query", type=str, metavar="DBTYPE", default=None, help="query a database")
    args = p.parse_args()

    timer = Timer()
    total = int(7.5 * 1024 * 1024)
    conditions = [battledb.Range("age", 18, 65), battledb.Range("income", 40000.0, 80000.)]

    if args.create is not None:
        if args.query is not None:
            print >>sys.stderr, "error:  can't specify both --create (-c) and --query (-q) together"
            return 1

        if args.create in ["sqlite"]:
            with timer:
                create_sql_db(args.create, "foobar.db", total, timer)
        elif args.create == "mongo":
            with timer:
                create_mongo_db(total, timer)
        else:
            print >>sys.stderr, "error:  unknown db type '%s' (legal choices: mongo, sqlite)" % (args.create)
            return 1
    elif args.query is not None:
        if args.create is not None:
            print >>sys.stderr, "error:  can't specify both --create (-c) and --query (-q) together"
            return 1

        if args.query in ["sqlite"]:
            with timer:
                results = query_sql_db(args.query, "foobar.db", conditions)
                count = results.count()
        elif args.query == "mongo":
            with timer:
                results = query_mongo_db(conditions)
                count = results.count()
        else:
            print >>sys.stderr, "error:  unknown db type '%s' (legal choices: mongo, sqlite)" % (args.query)
            return 1

        print >>sys.stderr, "found %d records" % (count)
    else:
        p.print_usage(sys.stderr)
        return 1

    print timer.seconds()

    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main())
