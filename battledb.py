import pymongo
import sqlalchemy


class Condition(object):
    def __init__(self, *args):
        raise RuntimeError("unimplemented")


class LT(Condition):
    def __init__(self, field, value):
        self.field = field
        self.value = value


class GTE(Condition):
    def __init__(self, field, value):
        self.field = field
        self.value = value


class EQ(Condition):
    def __init__(self, field, value):
        self.field = field
        self.value = value


class Range(Condition):
    def __init__(self, field, low, high):
        self.field = field
        self.low = low
        self.high = high


class DB(object):
    def connect(self):
        raise RuntimeError("unimplemented")

    def insert(self, record):
        raise RuntimeError("unimplemented")

    def commit(self):
        raise RuntimeError("unimplemented")

    def query(self, *conditions):
        raise RuntimeError("unimplemented")

    @staticmethod
    def render_condition(self, condition):
        raise RuntimeError("unimplemented")


class Mongo(DB):
    def __init__(self, hostname="localhost", port=27017, db="foo", collection="bar"):
        self.hostname = hostname
        self.port = port
        self.db = db
        self.collection = collection

        self.conn = None
        self.records = []

    def connect(self):
        self.conn = pymongo.MongoClient(self.hostname, self.port)[self.db][self.collection]

    def insert(self, record):
        self.records.append(record)

    def commit(self):
        self.conn.insert(self.records)
        self.records = []

    def query(self, *conditions):
        spec = {"$and": sum(map(self.render_condition, conditions), [])}
        return self.conn.find(spec)

    def render_condition(self, cond):
        if isinstance(cond, LT):
            return [{cond.field: {"$lt": cond.value}}]
        elif isinstance(cond, GTE):
            return [{cond.field: {"$gte": cond.value}}]
        elif isinstance(cond, EQ):
            return [{cond.field: cond.value}]
        elif isinstance(cond, Range):
            return self.render_condition(GTE(cond.field, cond.low)) + self.render_condition(LT(cond.field, cond.high))
        else:
            raise RuntimeError("illegal class instance for render_condition")


class SQL(DB):
    def __init__(self, dbtype, filename, record_cls):
        self.dbtype = dbtype
        self.filename = filename
        self.record_cls = record_cls

    def connect(self):
        self.engine = sqlalchemy.create_engine("%s:///%s" % (self.dbtype, self.filename))
        sessionmaker = sqlalchemy.orm.sessionmaker(bind=self.engine)
        self.session = sessionmaker()

    def insert(self, record):
        self.session.add(record)

    def commit(self):
        self.session.commit()

    def query(self, *conditions):
        constraint = sum(map(self.render_condition, conditions), [])
        return self.session.query(self.record_cls)\
                           .filter(*constraint)

    def render_condition(self, cond):
        if isinstance(cond, LT):
            return [eval("self.record_cls.%s" % (cond.field)) < cond.value]
        elif isinstance(cond, GTE):
            return [eval("self.record_cls.%s" % (cond.field)) >= cond.value]
        elif isinstance(cond, EQ):
            return [eval("self.record_cls.%s" % (cond.field)) == cond.value]
        elif isinstance(cond, Range):
            return self.render_condition(GTE(cond.field, cond.low)) + self.render_condition(LT(cond.field, cond.high))
        else:
            raise RuntimeError("illegal class instance for render_condition")
